// Copyright 2019 Tesla Motors Club LLC. All rights reserved.

package tmc

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func (o *Import) importGroups() error {
	doImport, ok := o.Config.Get("tmc.import_groups").(bool)
	if !ok {
		return errors.New("could not find value for 'tmc.import_groups' in config")
	}

	if !doImport {
		return nil
	}

	snogGroupsCategoryNodeID, ok := o.Config.Get("tmc.fc_groups_category_node_id").(int64)
	if !ok {
		return errors.New("could not find value for 'tmc.fc_groups_category_node_id' in config")
	}

	xf2RootDir, ok := o.Config.Get("xf2.xenforo_directory").(string)
	if !ok {
		return errors.New("could not find value for 'xf2.xenforo_directory' in config")
	}

	stmtSnogGroupsInsert, err := xf2.Txn1.Prepare(`
		INSERT INTO xf_snog_groups
		  (groupid, owner_id, name, groupdescription, shortdescription, hasforum, hasevent, hasmedia, privategroup, privatehide, groupavatar, groupbanner, node_id, category_id, media_id, membercount, threadcount, postcount)
		VALUES
		  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?, ?)
		ON DUPLICATE KEY UPDATE
		  groupid = VALUES(groupid),
		  owner_id = VALUES(owner_id),
		  name = VALUES(name),
		  groupdescription = VALUES(groupdescription),
		  shortdescription = VALUES(shortdescription),
		  hasforum = VALUES(hasforum),
		  hasevent = VALUES(hasevent),
		  hasmedia = VALUES(hasmedia),
		  privategroup = VALUES(privategroup),
		  privatehide = VALUES(privatehide),
		  groupavatar = VALUES(groupavatar),
		  groupbanner = VALUES(groupbanner),
		  node_id = VALUES(node_id),
		  category_id = VALUES(category_id),
		  media_id = VALUES(media_id),
		  membercount = VALUES(membercount),
		  threadcount = VALUES(threadcount),
		  postcount = VALUES(postcount)
	`)
	if err != nil {
		return err
	}

	stmtSnogGroupsEventsInsert, err := xf2.Txn1.Prepare(`
		INSERT INTO xf_snog_groups_events
		  (event_id, groupid, user_id, public, title, description, start, end, allDay)
		VALUES
		  (?, ?, ?, ?, ?, ?, ?, ?, 0)
		ON DUPLICATE KEY UPDATE
		  event_id = VALUES(event_id),
		  groupid = VALUES(groupid),
		  user_id = VALUES(user_id),
		  public = VALUES(public),
		  title = VALUES(title),
		  description = VALUES(description),
		  start = VALUES(start),
		  end = VALUES(end),
		  allDay = VALUES(allDay)
	`)
	if err != nil {
		return err
	}

	stmtSnogGroupMembersInsert, err := xf2.Txn1.Prepare(`
		INSERT INTO xf_snog_groups_members
		  (user_id, groups, moderator, owner, pending, invites)
		VALUES
		  (?, ?, ?, ?, ?, ?)
		ON DUPLICATE KEY UPDATE
		  user_id = VALUES(user_id),
		  groups = VALUES(groups),
		  moderator = VALUES(moderator),
		  owner = VALUES(owner),
		  pending = VALUES(pending),
		  invites = VALUES(invites)
	`)
	if err != nil {
		return err
	}

	stmtSnogGroupsUpdateMemberCount, err := xf2.Txn1.Prepare(`
		UPDATE xf_snog_groups
		SET membercount = ?
		WHERE groupid = ?
	`)
	if err != nil {
		return err
	}

	stmtMediaCategoryInsert, err := xf2.Txn1.Prepare(`
		INSERT INTO xf_mg_category
		  (category_id, title, description, breadcrumb_data, allowed_types, media_count, field_cache)
		VALUES
		  (?, ?, ?, 'a:0:{}', 'a:1:{i:0;s:5:"image";}', ?, 'a:0:{}')
		ON DUPLICATE KEY UPDATE
		  category_id = VALUES(category_id),
		  title = VALUES(title),
		  description = VALUES(description),
		  breadcrumb_data = VALUES(breadcrumb_data),
		  allowed_types = VALUES(allowed_types),
		  media_count = VALUES(media_count),
		  field_cache = VALUES(field_cache)
	`)
	if err != nil {
		return err
	}

	var (
		groupRows *sql.Rows

		groupID          int
		groupName        string
		groupDescription string
		groupType        int
		ownerID          int
		shortdescription string
		groupSettings    string
		groupState       string
		nodeID           int
		hasAvatar        bool
		terms            string
		location         string
		threadCount      int
		postCount        int
	)

	groupRows, err = xf2.Txn2.Query(`
		SELECT fcg.group_id AS groupid,
		       group_title AS name,
		       group_desc AS groupdescription,
		       group_type,
		       user_id AS owner_id, 
		       tag_line AS shortdescription,
		       group_settings, -- PHP serialize()'d
		       group_state, -- visible, moderated, deleted
			   forum_id AS node_id,
			   IF (avatar LIKE '%s:12:"avatar_width";i:0;%', 0, 1) AS hasAvatar,
		       term_service AS terms,
			   COALESCE(location, '') AS location,
			   COALESCE(xf_forum.discussion_count, 0) AS threadcount,
			   COALESCE(xf_forum.message_count, 0) AS postcount
		FROM fc_groups fcg
		LEFT JOIN xf_forum ON xf_forum.node_id = fcg.forum_id
	`)
	if err != nil {
		return err
	}

	var groupIDIsPrivateMap = map[int]bool{}
	var groupToOwnerMap = map[int]int{}
	var albumIDs = []int{}

	for groupRows.Next() {
		err = groupRows.Scan(&groupID, &groupName, &groupDescription, &groupType, &ownerID, &shortdescription, &groupSettings, &groupState, &nodeID, &hasAvatar, &terms, &location, &threadCount, &postCount)
		if err != nil {
			return err
		}

		hasForum := strings.Contains(groupSettings, `s:18:"enable_group_forum";s:1:"1";`)
		hasEvent := strings.Contains(groupSettings, `s:6:"events";s:1:"1";`)
		hasMedia := strings.Contains(groupSettings, `s:5:"media";s:1:"1";`)

		isPrivate := false
		isSecret := false

		switch groupType {
		case 1: // private
			isPrivate = true
		case 2: // secret
			isPrivate = true
			isSecret = true
		}

		groupIDIsPrivateMap[groupID] = isPrivate
		groupToOwnerMap[groupID] = ownerID

		// get the media album id for this group from xf1
		xfmgAlbumID, err := getGroupGalleryID(groupID)
		if err != nil {
			return err
		}
		albumIDs = append(albumIDs, xfmgAlbumID)

		groupBanner := "default.png"
		if hasAvatar {
			if _, err := os.Stat(xf2RootDir + "data/groups/GroupBanners/b/0/" + strconv.Itoa(groupID) + ".jpg"); err == nil {
				groupBanner = fmt.Sprintf("%d.jpg", groupID)
			}
		}

		_, err = stmtSnogGroupsInsert.Exec(groupID, ownerID, groupName, groupDescription, shortdescription, hasForum, hasEvent, hasMedia, isPrivate, isSecret, "", groupBanner, nodeID, snogGroupsCategoryNodeID, xfmgAlbumID, threadCount, postCount)
		if err != nil {
			return err
		}
	}

	var (
		eventRows *sql.Rows

		eventID      int
		eventTitle   string
		eventDesc    string
		contact      string
		userID       int
		eventDate    int
		eventEndDate int
		limit        string
		venue        string
		addr1        string
		addr2        string
		city         string
		state        string
		zip          string
		country      string
		latitude     string
		longitude    string
	)

	eventRows, err = xf2.Txn2.Query(`
		SELECT fc_e.event_id,
		       fc_e.event_title,
		       fc_e.event_desc,
		       fc_e.contact,
		       fc_e.group_id,
		       fc_e.user_id,
		       fc_e.event_date,
		       fc_e.event_end_date,
			   fc_e.limit,
			   COALESCE(fc_ea.venue, ''),
			   COALESCE(fc_ea.addr1, ''),
			   COALESCE(fc_ea.addr2, ''),
			   COALESCE(fc_ea.city, ''),
			   COALESCE(fc_ea.state, ''),
			   COALESCE(fc_ea.zip, ''),
			   COALESCE(fc_ea.country, ''),
			   COALESCE(fc_ea.lat, ''),
			   COALESCE(fc_ea.lng, '')
		FROM fc_events fc_e
		LEFT JOIN fc_event_addresses fc_ea
		  ON fc_ea.address_id = fc_e.address_id
	`)
	if err != nil {
		return err
	}

	for eventRows.Next() {
		err = eventRows.Scan(&eventID, &eventTitle, &eventDesc, &contact, &groupID, &userID, &eventDate, &eventEndDate, &limit, &venue, &addr1, &addr2, &city, &state, &zip, &country, &latitude, &longitude)
		if err != nil {
			return err
		}

		isPrivateEvent, ok := groupIDIsPrivateMap[groupID]
		if !ok {
			isPrivateEvent = true
		}

		var contactName, contactEmail, contactPhone string

		nameRe := regexp.MustCompile(`s:4:"name";s:[0-9]*:"(.*?)"`)
		nameMatches := nameRe.FindAllStringSubmatch(contact, 1)
		if len(nameMatches) != 1 || len(nameMatches[0]) != 2 {
			contactName = ""
		} else {
			contactName = nameMatches[0][1]
		}

		emailRe := regexp.MustCompile(`s:5:"email";s:[0-9]*:"(.*?)"`)
		emailMatches := emailRe.FindAllStringSubmatch(contact, 1)
		if len(emailMatches) != 1 || len(emailMatches[0]) != 2 {
			contactEmail = ""
		} else {
			contactEmail = emailMatches[0][1]
		}

		phoneRe := regexp.MustCompile(`s:5:"phone";s:[0-9]*:"(.*?)"`)
		phoneMatches := phoneRe.FindAllStringSubmatch(contact, 1)
		if len(phoneMatches) != 1 || len(phoneMatches[0]) != 2 {
			contactPhone = ""
		} else {
			contactPhone = phoneMatches[0][1]
		}

		if limit == "0" || limit == "" {
			limit = "N/A"
		}

		// a template that prepends the teble BB code to the event description
		eventDescription := fmt.Sprintf(`
[TABLE]
[TR]
[TD][B]Contact Name[/B][/TD]
[TD]%s[/TD]
[/TR]
[TR]
[TD][B]Contact Email[/B][/TD]
[TD]%s[/TD]
[/TR]
[TR]
[TD][B]Contact Phone[/B][/TD]
[TD]%s[/TD]
[/TR]
[TR]
[TD][B]Attendance Limit[/B][/TD]
[TD]%s[/TD]
[/TR]
[TR]
[TD][B]Venue[/B][/TD]
[TD]%s[/TD]
[/TR]
[TR]
[TD][B]Address[/B][/TD]
[TD]%s[/TD]
[/TR]
[TR]
[TD][B]City[/B][/TD]
[TD]%s[/TD]
[/TR]
[TR]
[TD][B]State, Postal/Zip Code, & Country[/B][/TD]
[TD]%s[/TD]
[/TR]
[/TABLE]

%s`, contactName, contactEmail, contactPhone, limit, venue, fmt.Sprintf("%s %s", addr2, addr1), city, fmt.Sprintf("%s %s %s", state, zip, country), eventDesc)

		_, err = stmtSnogGroupsEventsInsert.Exec(eventID, groupID, userID, !isPrivateEvent, eventTitle, eventDescription, eventDate, eventEndDate)
		if err != nil {
			return err
		}
	}

	var (
		membershipRows *sql.Rows
		// groupID int
		// userID int
		userState string
		role      int
	)

	type snogGroupMember struct {
		UserID    int
		Groups    []int
		Moderator []int
		Owner     []int
		Pending   []int
		Invites   []int
	}

	var groupMemberCount = map[int]int{}
	var userMembershipMap = map[int]*snogGroupMember{}

	membershipRows, err = xf2.Txn2.Query(`
		SELECT group_id,
			   user_id,
			   user_state,
			   role
		FROM fc_users_groups
		ORDER BY user_id, group_id
	`)

	for membershipRows.Next() {
		err = membershipRows.Scan(&groupID, &userID, &userState, &role)

		_, ok := userMembershipMap[userID]
		if !ok {
			userMembershipMap[userID] = &snogGroupMember{UserID: userID}
		}

		var addToGroup, addAsModerator, addAsOwner, addPending, addInvites bool

		if userState == "valid" {
			groupMemberCount[groupID]++

			switch role {
			case 1, 8, 6, 5:
				addToGroup = true
			case 2, 7, 4:
				addToGroup = true
				addAsModerator = true
			case 3, 9:
				addToGroup = true
				addAsModerator = true
				addAsOwner = true
			}

		} else if userState == "waiting" {
			addInvites = true
		} else if userState == "moderated" {
			addPending = true
		}

		if groupToOwnerMap[groupID] == userID {
			addAsOwner = true
		}

		gm := userMembershipMap[userID]

		if addToGroup {
			gm.Groups = append(gm.Groups, groupID)
		}

		if addAsModerator {
			gm.Moderator = append(gm.Moderator, groupID)
		}

		if addAsOwner {
			gm.Owner = append(gm.Owner, groupID)
		}

		if addInvites {
			gm.Invites = append(gm.Invites, groupID)
		}

		if addPending {
			gm.Pending = append(gm.Pending, groupID)
		}
	}

	for userID, gmp := range userMembershipMap {
		_, err = stmtSnogGroupMembersInsert.Exec(userID, phpSerializeIntArray(gmp.Groups), phpSerializeIntArray(gmp.Moderator), phpSerializeIntArray(gmp.Owner), phpSerializeIntArray(gmp.Pending), phpSerializeIntArray(gmp.Invites))
		if err != nil {
			return err
		}
	}

	// fix stats
	for groupID, memberCount := range groupMemberCount {
		// membercount
		_, err = stmtSnogGroupsUpdateMemberCount.Exec(memberCount, groupID)
		if err != nil {
			return err
		}
	}

	// todo:
	// threadcount
	// postcount
	// discussioncount: 0
	// photocount: 0

	// convert the groups albums to xfmg categories
	// and point the media to them
	var (
		albumRows *sql.Rows

		albumID     int
		title       string
		description string
		mediaCount  int
	)

	albumRows, err = xf2.Txn2.Query(`
		SELECT album_id,
			   title,
			   description,
			   media_count
		FROM xf_mg_album
		WHERE album_id IN (` + strings.Trim(strings.Replace(fmt.Sprint(albumIDs), " ", ",", -1), "[]") + `)
	`)
	if err != nil {
		return err
	}

	for albumRows.Next() {
		err = albumRows.Scan(&albumID, &title, &description, &mediaCount)
		if err != nil {
			return err
		}

		_, err = stmtMediaCategoryInsert.Exec(fmt.Sprintf(`100%d`, albumID), title, description, mediaCount)
		if err != nil {
			return err
		}
	}

	// point the media items to the categories
	_, err = xf2.Txn1.Exec(`
		UPDATE xf_mg_media_item
		SET category_id = CONCAT("100", album_id)
		WHERE album_id <> 0
		  AND album_id IN (` + strings.Trim(strings.Replace(fmt.Sprint(albumIDs), " ", ",", -1), "[]") + `)
	`)

	_, err = xf2.Txn1.Exec(`
		UPDATE xf_mg_media_item
		SET album_id = 0
		WHERE album_id <> 0
		  AND album_id IN (` + strings.Trim(strings.Replace(fmt.Sprint(albumIDs), " ", ",", -1), "[]") + `)
	`)

	return err
}

func getGroupGalleryID(groupID int) (int, error) {
	var xengalleryAlbumRows *sql.Rows
	var albumID int

	xengalleryAlbumRows, err := xf1.Txn.Query(`SELECT album_id FROM xengallery_album WHERE group_id = ?`, groupID)
	if err != nil {
		return albumID, err
	}

	for xengalleryAlbumRows.Next() {
		err = xengalleryAlbumRows.Scan(&albumID)
	}

	return albumID, err
}

func phpSerializeIntArray(numbers []int) string {

	serializedNumbers := ""
	for i, num := range numbers {
		numberStr := strconv.Itoa(num)
		serializedNumbers = serializedNumbers + fmt.Sprintf(`i:%d;s:%d:"%s";`, i, len(numberStr), numberStr)
	}

	return fmt.Sprintf(`a:%d:{%s}`, len(numbers), serializedNumbers)
}
