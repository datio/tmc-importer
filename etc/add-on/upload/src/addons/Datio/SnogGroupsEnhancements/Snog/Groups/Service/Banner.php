<?php

namespace Datio\SnogGroupsEnhancements\Snog\Groups\Service;

class Banner extends XFCP_Banner
{
	protected $bannersizeMap = ['o' => 10000, 'b' => 275, 'f' => 917, 'a' => 80];
	protected $heightMap = ['o' => 10000, 'b' => 275, 'f' => 300, 'a' => 80];
}