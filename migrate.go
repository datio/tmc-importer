// Copyright 2019 Tesla Motors Club LLC. All rights reserved.

package tmc

import (
	"io/ioutil"
	"os"
)

func (o *Import) migrate() error {
	if err = o.importGroups(); err != nil {
		return err
	}

	return xf2.Txn1.Commit()
}

func (o *Import) createEmptyDirIfNotExists(dirPath string, createEmptyHTLMFile bool) error {
	_, err = os.Stat(dirPath)
	if os.IsNotExist(err) {
		if err = os.MkdirAll(dirPath, 0755); err != nil {
			return err
		}
	} else {
		return nil
	}

	// XenForo index.html files include a space.
	if createEmptyHTLMFile {
		err = ioutil.WriteFile(dirPath+"index.html", []byte(" "), 0644)
	}

	return err
}
